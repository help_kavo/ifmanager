package controller.utility.alerts.fm;

import javafx.scene.control.Alert;

/**
 * <p>Класс предназначен для метода диалогового окна кнопки перемещения и копирования файла/директории</p>
 *
 * @author Волков Валерий, 16IT18k
 */
public class NewWindowButtonAlerts {

    /**
     * Метод вызывает диалоговое окно предупреждение, если в актуальной таблице
     * файлов не выбран файл, над которым нужно совершить операцию, а пользователь нажимает на операцию
     * копирования/перемещения.
     *
     * Это необходимо для того, чтобы пользователю не выводилось новое окно приложения, когда была выбрана операция,
     * а выдовалось предупреждение, в результате чего не произойдёт ошибка.
     */
    public static void newFileWarning() {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Warning!");
        alert.setHeaderText("No a file is selected for copying or moving!");
        alert.setContentText("Please select a file in the table.");
        alert.showAndWait();
    }
}