package controller.utility.alerts.fm;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.util.Optional;

/**
 * <p>Класс предназначен для хранения методов диалогового окна для различных ситуаций
 * с кнопкой разархивации</p>
 *
 * @author Волков Валерий, 16IT18k
 */
public class UnarchiveButtonAlerts {

    /**
     * Метод вызывает диалоговое окно предупреждение, если в актуальной таблице
     * файлов не выбран Zip-файл, а пользователь нажимает на операцию разархивации
     */
    public static void unarchiveWarning() {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Warning!");
        alert.setHeaderText("No a Zip-file is selected for to unzip!");
        alert.setContentText("Please select a Zip-file in the table.");
        alert.showAndWait();
    }

    /**
     * Метод вызывает диалоговое окно подтверждение операции разархивации и ожидает
     * наступления какого-либо события
     *
     * @return ожидание события
     */
    public static Optional<ButtonType> unarchiveConfirm() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation!");
        alert.setHeaderText("Unzip!");
        alert.setContentText("You are going to unarchive the Zip-file?");
        return alert.showAndWait();
    }

    /**
     * Метод вызывает диалоговое окно информацию о успешной разархивации
     * Zip-файла
     */
    public static void unarchiveInformation() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information!");
        alert.setHeaderText("Successfully!");
        alert.setContentText("The Zip-file is successfully unzipped");
        alert.showAndWait();
    }
}