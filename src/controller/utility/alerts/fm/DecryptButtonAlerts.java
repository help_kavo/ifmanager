package controller.utility.alerts.fm;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.util.Optional;

/**
 * <p>Класс предназначен для хранения методов диалогового окна для различных ситуаций
 * с кнопкой расшифрования</p>
 *
 * @author Волков Валерий, 16IT18k
 */
public class DecryptButtonAlerts {

    /**
     * Метод вызывает диалоговое окно предупреждение, если в актуальной таблице
     * файлов не выбран файл, а пользователь нажимает на операцию расшифрования
     */
    public static void decryptWarning() {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Warning!");
        alert.setHeaderText("No a file is selected for decryption!");
        alert.setContentText("Please select a file in the table.");
        alert.showAndWait();
    }

    /**
     * Метод вызывает диалоговое окно подтверждение операции расшифрования и ожидает
     * наступления какого-либо события
     *
     * @return ожидание события
     */
    public static Optional<ButtonType> decryptConfirm() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation!");
        alert.setHeaderText("Decrypting!");
        alert.setContentText("You are going to decrypt the file?");
        return alert.showAndWait();
    }

    /**
     * Метод вызывает диалоговое окно информацию о успешном расшифровании файла
     */
    public static void decryptInformation() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information!");
        alert.setHeaderText("Successfully!");
        alert.setContentText("The file is successfully decrypted");
        alert.showAndWait();
    }
}