package controller.utility.alerts.fm;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.util.Optional;

/**
 * <p>Класс предназначен для хранения методов диалогового окна для различных ситуаций
 * с кнопкой перемещения</p>
 *
 * @author Волков Валерий, 16IT18k
 */
public class MoveButtonAlerts {

    /**
     * Метод вызывает диалоговое окно предупреждение, если в актуальной таблице
     * файлов не выбран файл, а пользователь нажимает на операцию перемещения
     */
    public static void moveWarning() {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Warning!");
        alert.setHeaderText("No a file or a directory is selected for moving!");
        alert.setContentText("Please select a file or a directory in the table.");
        alert.showAndWait();
    }

    /**
     * Метод вызывает диалоговое окно подтверждение операции перемещения и ожидает
     * наступления какого-либо события
     *
     * @return ожидание события
     */
    public static Optional<ButtonType> moveConfirm() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation!");
        alert.setHeaderText("Moving!");
        alert.setContentText("You are going to move the file?");
        return alert.showAndWait();
    }

    /**
     * Метод вызывает диалоговое окно информацию о успешном перемещении директории
     */
    public static void moveDirInformation() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information!");
        alert.setHeaderText("Successfully!");
        alert.setContentText("The directory is successfully moved");
        alert.showAndWait();
    }

    /**
     * Метод вызывает диалоговое окно информацию о успешном перемещении файла
     */
    public static void moveFileInformation() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information!");
        alert.setHeaderText("Successfully!");
        alert.setContentText("The file is successfully moved");
        alert.showAndWait();
    }
}