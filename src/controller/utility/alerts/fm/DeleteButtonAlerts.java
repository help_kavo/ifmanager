package controller.utility.alerts.fm;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.util.Optional;

/**
 * <p>Класс предназначен для хранения методов диалогового окна для различных ситуаций
 * с кнопкой удаления</p>
 *
 * @author Волков Валерий, 16IT18k
 */
public class DeleteButtonAlerts {

    /**
     * Метод вызывает диалоговое окно предупреждение, если в актуальной таблице
     * файлов не выбрана ни директория, ни файл, а пользователь нажимает на операцию
     * удаления
     */
    public static void deleteWarning() {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Warning!");
        alert.setHeaderText("No a file or a directory is selected for deletion!");
        alert.setContentText("Please select a file or a directory in the table.");
        alert.showAndWait();
    }

    /**
     * Метод вызывает диалоговое окно подтверждение операции удаления и ожидает
     * наступления какого-либо события
     *
     * @return ожидание события
     */
    public static Optional<ButtonType> deleteConfirm() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation!");
        alert.setHeaderText("Deletion!");
        alert.setContentText("You are going to delete the file or the directory?");
        return alert.showAndWait();
    }

    /**
     * Метод вызывает диалоговое окно информацию о успешном удалении
     * директории
     */
    public static void deleteDirInformation() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information!");
        alert.setHeaderText("Successfully!");
        alert.setContentText("The directory is successfully deleted");
        alert.showAndWait();
    }

    /**
     * Метод вызывает диалоговое окно информацию о успешном удалении
     * файла
     */
    public  static  void deleteFileInformation() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information!");
        alert.setHeaderText("Successfully!");
        alert.setContentText("The file is successfully deleted");
        alert.showAndWait();
    }
}