package controller.utility.alerts.fm;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.util.Optional;

/**
 * <p>Класс предназначен для хранения методов диалогового окна для различных ситуаций
 * с кнопкой создания</p>
 *
 * @author Волков Валерий, 16IT18k
 */
public class CreateButtonAlerts {

    /**
     * Метод вызывает диалоговое окно подтверждение операции создания файла или директории и ожидает
     * наступления какого-либо события
     *
     * @return ожидание события
     */
    public static Optional<ButtonType> createConfirm() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation!");
        alert.setHeaderText("Creating!");
        alert.setContentText("You are going to create the file or the directory?");
        return alert.showAndWait();
    }

    /**
     * Метод вызывает диалоговое окно информацию о успешном создании директории
     */
    public static void createDirInformation() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information!");
        alert.setHeaderText("Successfully!");
        alert.setContentText("The directory is successfully created");
        alert.showAndWait();
    }

    /**
     * Метод вызывает диалоговое окно информацию о успешном создании файла
     */
    public static void createFileInformation() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information!");
        alert.setHeaderText("Successfully!");
        alert.setContentText("The file is successfully created");
        alert.showAndWait();
    }
}