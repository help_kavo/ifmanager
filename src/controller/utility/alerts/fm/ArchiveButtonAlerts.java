package controller.utility.alerts.fm;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.util.Optional;

/**
 * <p>Класс предназначен для хранения методов диалогового окна для различных ситуаций
 * с кнопкой архивации</p>
 *
 * @author Волков Валерий, 16IT18k
 */
public class ArchiveButtonAlerts {

    /**
     * Метод вызывает диалоговое окно предупреждение, если в актуальной таблице
     * файлов не выбрана ни директория, ни файл, а пользователь нажимает на операцию
     * архивации
     */
    public static void archiveWarning() {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Warning!");
        alert.setHeaderText("No a file or a directory is selected for archiving!");
        alert.setContentText("Please select a file or a directory in the table.");
        alert.showAndWait();
    }

    /**
     * Метод вызывает диалоговое окно подтверждение операции архивации и ожидает
     * наступления какого-либо события
     *
     * @return ожидание события
     */
    public static Optional<ButtonType> archiveConfirm() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation!");
        alert.setHeaderText("Archiving!");
        alert.setContentText("You are going to archive the file or the directory?");
        return alert.showAndWait();
    }

    /**
     * Метод вызывает диалоговое окно информацию о успешной архивации
     * директории
     */
    public static void archiveDirInformation() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information!");
        alert.setHeaderText("Successfully!");
        alert.setContentText("The directory is successfully archived");
        alert.showAndWait();
    }

    /**
     * Метод вызывает диалоговое окно информацию о успешной архивации
     * файла
     */
    public static void archiveFileInformation() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information!");
        alert.setHeaderText("Successfully!");
        alert.setContentText("The file is successfully archived");
        alert.showAndWait();
    }
}