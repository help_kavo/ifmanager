package controller.utility.alerts.fm;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.util.Optional;

/**
 * <p>Класс предназначен для хранения методов диалогового окна для различных ситуаций
 * с кнопкой переименования</p>
 *
 * @author Волков Валерий, 16IT18k
 */
public class RenameButtonAlerts {

    /**
     * Метод вызывает диалоговое окно предупреждение, если в актуальной таблице
     * файлов не выбран файл, а пользователь нажимает на операцию переименования
     */
    public static void renameWarning() {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Warning!");
        alert.setHeaderText("No a file is selected for renaming!");
        alert.setContentText("Please select a file in the table.");
        alert.showAndWait();
    }

    /**
     * Метод вызывает диалоговое окно подтверждение операции переименования и ожидает
     * наступления какого-либо события
     *
     * @return ожидание события
     */
    public static Optional<ButtonType> renameConfirm() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation!");
        alert.setHeaderText("Renaming!");
        alert.setContentText("You are going to rename the file?");
        return alert.showAndWait();
    }

    /**
     * Метод вызывает диалоговое окно информацию о успешном переименовании файла
     */
    public static void renameInformation() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information!");
        alert.setHeaderText("Successfully!");
        alert.setContentText("The file is successfully renamed");
        alert.showAndWait();
    }
}