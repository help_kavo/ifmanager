package controller.utility;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Класс предназначен для манипуляции путями в программе.
 *
 * @author Проничкин Вячеслав, 16IT18k
 */
public class FileController {

    private String file;
    private Path filePath;

    private String fileDestination;
    private Path fileDestinationPath;

    /**
     * @param file - Абсолютный путь в строковом типе данных
     */
    public FileController(String file) {
        this.file = file;
        filePath = Paths.get(file);
    }

    public String getFile() {
        return file;
    }

    public Path getPath() { return filePath; }

    public String getFileDestination() { return fileDestination; }

    public Path getDestinationPath() { return fileDestinationPath; }

    public void setFile(String file) {
        this.file = file;
    }

    public void setDestinationPath(Path fileDestinationPath) { this.fileDestinationPath = fileDestinationPath; }

    public void setDestination(String fileDestination) {
        this.fileDestination = fileDestination;
        fileDestinationPath = Paths.get(fileDestination);
    }
}