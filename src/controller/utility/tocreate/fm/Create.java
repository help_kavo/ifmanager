package controller.utility.tocreate.fm;

import controller.utility.FileController;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * класс для создания файлов и деректории
 *
 * функционал методов для работы с исходным кодом:
 * <ul>
 *  <li> создание файла в указаной директории </li>
 *  <li> создание директории с проверкой на наличие существующей директории </li>
 * </ul>
 *
 * @author Гусаров Евгений, 16IT18k
 */

 public class Create {
    /**
     * @param paths обьект класса PathController в нем хранятся начальный и конечный путь.
     * @param newFIle название нового файла.
     *                метод для создания файла в директории
     */
    public static void oneFileCreation(FileController paths, String newFIle) {
        if (Files.exists(paths.getPath())) // если файл существует выполняется код
        {
            try {
                Files.createFile(Paths.get(paths.getPath() + "\\" + newFIle));//создание нового файла
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * @param paths обьект класса PathController в нем хранятся начальный и конечный путь.
     * @param newPackage название новой директории.
     *                   метод для создания директории
     */
    public static void onePackageCreate(FileController paths, String newPackage) {
        if (Files.exists(paths.getPath())) {
            try {
                Files.createDirectory(Paths.get(paths.getPath() + "\\" + newPackage));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}