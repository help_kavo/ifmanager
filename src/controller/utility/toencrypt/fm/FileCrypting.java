package controller.utility.toencrypt.fm;

import controller.utility.FileController;
import controller.utility.GetFilename;

import java.io.File;

/**
 * Класс, шифрующий одиночные файлы
 *
 * Функционал методов для работы с исходным кодом:
 * <ul>
 * <li> Шифрование файла путём шифрования AHS</li>
 * <li> Для зашифровки и расшифровки требуется ключ</li>
 * </ul>
 *
 * @author Проничкин Вячеслав, 16IT18k
 */

public class FileCrypting {

    static String key = "5432131235434433";

    //GetFilename extSeparator = new GetFilename(); //Экземпляр класса GetFilename для получения имени файла без его расширения.

    /**
     * @param paths Объект класса FileController, который содержит в себе пути
     *
     *              Метод зашифровывает файл, используя ключ key. Стирает расширения файла, давая ему
     *              новое расширение ".encrypted"
     *
     */
    public static void fileEncrypt(FileController paths) {

        String woExtension = GetFilename.filename(paths);

        File inputFile = new File(String.valueOf(paths.getPath()));
        File encryptedFile = new File(woExtension + ".encrypted");
        try {

            CryptoUtils.encrypt(key, inputFile, encryptedFile);

        } catch (CryptoException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
    }

    /**
     * @param paths Объект класса FileController, который содержит в себе пути
     *
     *              Метод расшифровывает зашифрованный файл с расширением ".encrypted" с помощью ключа
     *              key. После расшифрования файлу присваивается новое расширение - ".decrypted"
     *
     */
    public static void fileDecrypt(FileController paths){

        String woExtension = GetFilename.filename(paths);
        File inputFile = new File(String.valueOf(paths.getPath()));

        File decryptedFile = new File(woExtension + ".decrypted");
        try {

            CryptoUtils.decrypt(key, inputFile, decryptedFile);

        } catch (CryptoException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
    }
}

