package controller.utility.tocopy.fm;

import controller.utility.FileController;

import java.io.IOException;
import java.nio.file.*;
/**
 * Функционал методов для работы с исходным кодом:
 * <ul>
 *     <li>Копирование файла из исходной папки в другую папку</li>
 * </ul>
 *
 * Класс предназначен копирования одного файла
 * @author Кожевников Дмитрий, 16IT18k
 */

public class Copy {

    /**
     * @param paths - Объект класса FileController, который содержит в себе пути
     *              Метод копирует одиночный файл из одной папки в другую папку.
     */
    static public void copyFile(FileController paths) {
        if (Files.exists(paths.getPath(), LinkOption.NOFOLLOW_LINKS) && !Files.isDirectory(paths.getPath(), LinkOption.NOFOLLOW_LINKS)) {
            try {
                Files.copy(paths.getPath(), Paths.get(paths.getDestinationPath() + "//" + paths.getPath().getFileName()), StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}



