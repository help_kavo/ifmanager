package controller.utility.tomove.fm;

import controller.utility.FileController;

import java.io.IOException;
import java.nio.file.*;

/**
 * Класс для переноса одного или всех файлов из каталога в указанный пользователем каталог
 *
 * Функционал методов для работы с исходным кодом:
 * <ul>
 *     <li>Перенос всех файлов из папки в другую папку</li>
 *     <li>Перенос одного файла из папки в другую папку</li>
 * </ul>
 *
 * Класс предназначен для разархивирования Zip архива
 * @author Волков Валерий, 16IT18k
 */
    public class Move {


        /**
         * @param paths - Объект класса FileController, который содержит в себе путь таргета и путь назначения
         *
         *              Метод для перемещения всех файлов из папки.
         */
        static public void dirMoving(FileController paths) {
            if (Files.exists(paths.getPath(), LinkOption.NOFOLLOW_LINKS) && Files.isDirectory(paths.getPath(), LinkOption.NOFOLLOW_LINKS)) {
                try (DirectoryStream<Path> stream = Files.newDirectoryStream(paths.getPath())) {
                    for (Path entry : stream) {
                        if (Files.exists(paths.getDestinationPath(), LinkOption.NOFOLLOW_LINKS) && Files.isDirectory(paths.getDestinationPath(), LinkOption.NOFOLLOW_LINKS)) {
                            Files.move(entry, Paths.get(paths.getDestinationPath() + "\\" + entry.getFileName()));
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    System.exit(1);
                }
            }
        }


        /**
         * @param paths - Объект класса FileController, который содержит в себе путь таргета и путь назначения.
         *
         *              Метод для перемещения одного файла.
         */
        static public void oneFileMoving(FileController paths) {
            if (Files.exists(paths.getPath(), LinkOption.NOFOLLOW_LINKS) && !Files.isDirectory(paths.getPath(), LinkOption.NOFOLLOW_LINKS)) {
                try {
                    if (Files.exists(paths.getDestinationPath(), LinkOption.NOFOLLOW_LINKS) && Files.isDirectory(paths.getDestinationPath(), LinkOption.NOFOLLOW_LINKS)) {
                        Files.copy(paths.getPath(), Paths.get(paths.getFileDestination() + "//" + paths.getPath().getFileName()), StandardCopyOption.REPLACE_EXISTING);
                        Files.delete(paths.getPath());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    System.exit(1);
                }
            }
        }
    }

