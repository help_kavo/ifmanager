package controller.utility.todelete.fm;

import controller.utility.FileController;

import java.io.IOException;
import java.nio.file.*;

/**
 * класс для удаления одиночных файлов и папок с подпапками и файлами в них
 *
 * функционал методов для работы с исходным кодом:
 * <ul>
 *  <li> Удаление одного файла </li>
 *  <li> Удаления папок с подпапками и файлами в них</li>
 * </ul>
 *
 * @author Проничкин Вячеслав, 16IT18k
 */

public class Delete {

    /**
     * @param paths Объект класса FileController, который содержит в себе пути
     *              Метод для удаления одного файла
     */
    static public void oneFileDelete(FileController paths){
        if (Files.exists(paths.getPath(), LinkOption.NOFOLLOW_LINKS) && !Files.isDirectory(paths.getPath(), LinkOption.NOFOLLOW_LINKS))
        {
            try
            {
                Files.delete(paths.getPath());
            }
            catch (IOException e)
            {
                e.printStackTrace();
                System.exit(1);
            }
        }
    }

    /**
     * @param paths Объект класса FileController, который содержит в себе пути
     *              Метод для удаления множества папок, подпапок и файлов, содержащихся в папках
     */
    static public void dirDelete(FileController paths){
        if (Files.exists(paths.getPath(), LinkOption.NOFOLLOW_LINKS) && Files.isDirectory(paths.getPath(), LinkOption.NOFOLLOW_LINKS))
        {
            try (DirectoryStream<Path> stream = Files.newDirectoryStream(paths.getPath()))
            {
                for (Path entry : stream)
                {
                    if(Files.isDirectory(entry)){
                        entryDelete(entry);
                    } else {
                        Files.delete(entry);
                    }
                }
                Files.delete(paths.getPath());
            }
            catch (IOException e)
            {
                e.printStackTrace();
                System.exit(1);
            }
        }
    }

    /**
     * @param getIn Путь, который обозначен циклом for each
     *              Рекурсивная функция для удаления файлов и папок из подпапок.
     */
    static public void entryDelete(Path getIn){
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(getIn))
        {
            for (Path entry : stream)
            {
                    Files.delete(entry);
            }
            Files.delete(getIn);
        }
        catch (IOException e)
        {
            e.printStackTrace();
            System.exit(1);
        }
    }

}

