package controller.utility;

import controller.utility.alerts.fm.CreateButtonAlerts;
import controller.utility.alerts.fm.RenameButtonAlerts;
import javafx.fxml.FXML;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import controller.utility.tocreate.fm.Create;
import controller.utility.torename.fm.Rename;

import java.util.Optional;

/**
 * Класс предназначен для работы с интерфейсом окна, предназначенного для создания
 * имён файлов или папок
 *
 * @author Проничин Вячеслав, 16IT18k
 */
public class SetNameController {

    @FXML
    private javafx.scene.control.Button exitButton, renameButton, createFileButton, createFolderButton;

    //Переменная для связи с текстовым полем
    @FXML
    private TextField nameArea;

    //Имя файла
    @FXML
    private String fileName = null;

    /**
     * Инициализация имени файла из текста, полученного из текстового поля
     */
    @FXML
    private void handleTextAreaAction() { //Переименовать метод
        fileName = nameArea.getText();
    }

    /**
     * Закрытие окна интерфейса
     */
    @FXML
    private void handleExitButton() {
        windowClose(exitButton);
    }

    /**
     * Переименование файла
     */
    @FXML
    private void handleRenameButton() {
        FileController fileControl = new FileController(Controller.selectedFile);
        fileName = nameArea.getText();

        Optional<ButtonType> result = RenameButtonAlerts.renameConfirm();
        if (result.get() == ButtonType.OK) {
            Rename.rename(fileControl, fileName);
            RenameButtonAlerts.renameInformation();
        }
        windowClose(renameButton);
        nameArea = null;
    }

    /**
     * Создание файла с новым именем
     */
    @FXML
    private void handleCreateFileButton() {
        FileController fileControl = new FileController(Controller.selectedDir);
        fileName = nameArea.getText();

        Optional<ButtonType> result = CreateButtonAlerts.createConfirm();
        if (result.get() == ButtonType.OK) {
            Create.oneFileCreation(fileControl, fileName);
            CreateButtonAlerts.createFileInformation();
        }

        windowClose(createFileButton);
        nameArea = null;
    }

    /**
     * Создание папки с новым именем
     */
    @FXML
    private void handleCreateFolderButton() {
        FileController fileControl = new FileController(Controller.selectedDir);
        fileName = nameArea.getText();

        Optional<ButtonType> result = CreateButtonAlerts.createConfirm();
        if (result.get() == ButtonType.OK) {
            Create.onePackageCreate(fileControl, fileName);
            CreateButtonAlerts.createDirInformation();
        }

        windowClose(createFolderButton);
        nameArea = null;
    }

    /**
     * @param button fx:id кнопки
     */
    private void windowClose(javafx.scene.control.Button button) {
        Stage stage = (Stage) button.getScene().getWindow();
        stage.close();
    }
}