package controller.utility.toarchive.fm;

import controller.utility.FileController;
import controller.utility.GetFilename;

import java.io.*;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Функционал методов для работы с исходным кодом:
 * <ul>
 *     <li> Разархивация Zip архива в указанную директорию</li>
 *     <li> Перезапись директории, если таковая уже существует в системе</li>
 *     <li> Разархивирование всех папок и подпапок с файлами, находящихся в указанной директории</li>
 * </ul>
 *
 * <p>Класс предназначен для разархивирования Zip архива - всех подпапок и папок с файлами
 * из архива</p>
 *
 * @author Волков Валерий, 16IT18k
 */

public class Unarchive {

    /**
     * Метод предназначен для разархивирования в указанную папку
     *
     * @param paths объект класса PathController, содеращий в себе начальный и конечный пути
     */
    public static void unZip(FileController paths){
        File folder = new File(GetFilename.filename(paths)); // filename - метод из класса GetFilename, удаляющий расширение файла
        if (!folder.exists()) {
            folder.mkdir();
        } else {
            folder.delete();
            folder.mkdir();
        }
        unzipDirsAndFilesInDir(paths, folder);
    }

    /**
     * Метод предназначен для разархивирования всех подпапок и папок с файлами из Zip архива
     *
     * @param paths объект класса PathController, содеращий в себе начальный и конечный пути
     * @param folder папка, в которую необходимо разархивировать содержимое Zip архива
     */
    private static void unzipDirsAndFilesInDir(FileController paths, File folder) {
        try (ZipFile zip = new ZipFile(String.valueOf(paths.getPath()))){ // Получаем содержимое Zip архива
            Enumeration entries = zip.entries(); //Для получения перечисления элементов оглавления архива

            while ((entries.hasMoreElements())) {
                ZipEntry entry = (ZipEntry) entries.nextElement(); //Возвращает описывающий запись объект типа ZipEntry

                String pathOfFile = entry.getName(); //Хранит в себе названия файлов - объёектов типа ZipEntry
                File relativePath = new File(folder + File.separator + pathOfFile); //Хранит в себе абсолютный путь разархивируемых файлов относительно папки, в которую будет разархивирован архив

                if (entry.isDirectory()) { // Если содержимое Zip архива является директорией - она создаётся
                    new File(String.valueOf(relativePath.mkdirs()));
                    continue;
                }
                new File(relativePath.getParent()).mkdirs(); // Создаеются все родительские директории

                try (FileOutputStream fos = new FileOutputStream(relativePath)) {
                    WriteBytes.write(zip.getInputStream(entry), fos);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}