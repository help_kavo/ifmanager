package controller.utility.toarchive.fm;

import java.io.*;

/**
 * <p>Класс предназначен для записи байтов информации</p>
 *
 * @author Волков Валерий, 16IT18k
 */
class WriteBytes {

    /**
     * Метод записывает в поток вывода массив байтов
     * @param input - поток ввода
     * @param output - поток вывода
     */
    static void write(InputStream input, OutputStream output){
        try {
            byte[] buffer = new byte[4096];
            int length;
            while ((length = input.read(buffer)) > 0) {
                output.write(buffer, 0, length);
            }
            input.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}