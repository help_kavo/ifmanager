package controller.utility.toarchive.fm;

import controller.utility.FileController;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Функционал методов для работы с исходным кодом:
 * <ul>
 *     <li> Создание Zip архива и запись в него файлов</li>
 *     <li> Архивация директории. Архивация директории так же
 *     подразумевает архивацию всех подпапок и папок с файлами,
 *     входящих в указанную директорию</li>
 *     <li> Архивация отдельного файла(не директории)</li>
 * </ul>
 *
 * <p>Класс предназначен для архивирования директории с файлами и для архивирования
 * отдельного файла</p>
 *
 * @author Волков Валерий, 16IT18k
 */
public class Archive {

    /**
     * Метод предназначен для создания Zip архива и записи в него файлов
     *
     * @param paths - объект класса PathController, содеращий в себе начальный и конечный пути
     * @throws IOException - ошибка ввода-вывода
     */
    public static void zipping(FileController paths) throws IOException {
        try (ZipOutputStream out = new ZipOutputStream(new FileOutputStream(paths.getPath() + ".zip"))) {

            File file = new File(String.valueOf(paths.getPath())); //Хранит путь до исходного файла

            if (file.isDirectory()) {
                addDirsAndFilesToArchive(file,out, paths);
            } else addFileToArchive(file, out, Paths.get(file.getName()));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод предназначен для добавления директорий с файлами в Zip архив
     *
     * @param dir - архивируемая директрория
     * @param out - поток ZipOutputStream
     * @param path - объект класса PathController, содеращий в себе начальный и конечный пути
     */
    private static void addDirsAndFilesToArchive(File dir, ZipOutputStream out, FileController path) {
        File[] arrayFiles = dir.listFiles();

        assert arrayFiles != null;
        for (File file : arrayFiles) {

            Path pathToDir = path.getPath(); //Переменная содержит путь изначального расположения директории, которую нужно заархивировать
            Path pathToFilesAndDirs = Paths.get(file.getAbsolutePath()); //Переменная содержит абсолюбный путь до файла, расположенного во вложенной директории
            Path relativePath = pathToDir.relativize(pathToFilesAndDirs); //Содержит относительный путь от архивируемой директории до файла во вложенной директории

            if (file.isDirectory()) {
                if (file.length() == 0) {
                    try {
                        out.putNextEntry(new ZipEntry(relativePath + "/"));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                addDirsAndFilesToArchive(file, out, path); //рекурсивно вызывается метод addDirsAndFilesToArchive
                continue;
            }
            addFileToArchive(file, out, relativePath);
        }
    }

    /**
     * Метод предназначен для добавления одного отдельного файла(не директории) в Zip архив, а так же
     * для добавления файлов, находящихся во вложенных директориях, если исходный файл является директорией
     * @param file - исходный архивируемый файл
     * @param out - поток ZipOutputStream
     * @param path - путь до: файла, если исходный файл - не директория; файлов, если исходный файл - директория
     */
    private static void addFileToArchive(File file, ZipOutputStream out, Path path) {
        try {
            out.putNextEntry(new ZipEntry(String.valueOf(path)));
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (FileInputStream fis = new FileInputStream(file)){
            WriteBytes.write(fis, out);
            out.closeEntry();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}