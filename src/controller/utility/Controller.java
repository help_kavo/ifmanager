package controller.utility;
import controller.utility.alerts.fm.*;
import controller.utility.toarchive.fm.Unarchive;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.Optional;

import controller.utility.toarchive.fm.Archive;
import controller.utility.todelete.fm.Delete;
import controller.utility.toencrypt.fm.FileCrypting;

/**
 * Класс, предназначен для работы с интерфейсом и локальной базой данных
 *
 * Функционал методов для работы с исходным кодом:
 * <ul>
 *     <li>Инициализация каталога файлов, поддержание его актуальности</li>
 *     <li>Методы, производящие некие действия при нажатии на определенные части интерфейса</li>
 *     <li>Открытие новых окон интерфейса</li>
 * </ul>
 *
 * @author Проничкин Вячеслав, 16IT18k
 */


public class Controller {

    //Создание локальной базы данных из каталога файлов по текущему пути
    private ObservableList<FileController> userData = FXCollections.observableArrayList();

    //Выбранный путь для просмотра каталога файлов
    public static String selectedDir = "E:\\test";

    //Выбранный файл для произведения с ним различных манипуляций
    public static String selectedFile;

    //Второй путь для тех методов, которые требуют конечный путь назначения для переноса\копирования файлов
    public static String selectedDestinationPath = null;

    //Переменная для связи с Label, чтобы отображать выбранный файл
    @FXML
    private Label selectedFileLabel;

    //Переменная для связи с текстовым полем
    @FXML
    private TextField pathArea;

    //Переменная для связи с таблицей
    @FXML
    private TableView<FileController> files;

    //Переменная для связи со столбцами в таблице
    @FXML
    private TableColumn<FileController, String> nameFile;

    /**
     *  Метод для инициализации каталога файлов, а так же поддержания его актуальности
     *  Под актуальностью имеется ввиду отображение нового каталога файлов при изменении
     *  текущего абсолютного пути
     */
    @FXML
    private void initialize() {
        initData();
        nameFile.setCellValueFactory(new PropertyValueFactory<>("file"));

        files.setItems(userData);
        clickEvent();
    }

    /**
     * Метод для перехода во вложенную папку по двойному щелчку или выбор
     * файла для манипуляций с ним по одиночному щелчку
     */
    private void clickEvent() {
        files.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
                    if (mouseEvent.getClickCount() == 2) {
                        if (files.getSelectionModel().getSelectedItem() != null) {
                            getSelectedRow();
                            File currentPath = new File(selectedFile);
                            if (Files.isDirectory(Paths.get(selectedDir + "\\" + currentPath.getName()))) {
                                selectedDir = selectedDir + "\\" + currentPath.getName();
                                refreshDirectory();
                            }
                        }
                    } else {
                        if (files.getSelectionModel().getSelectedItem() != null) {
                            getSelectedRow();
                            File currentPath = new File(selectedFile);
                            selectedFileLabelInit(currentPath);
                        }
                    }
                }
            }
        });
    }

    /**
     * Нахождение и получение той информации, которая находится в ячейке таблицы,
     * на которую кликнули мышкой.
     */
    private void getSelectedRow() {
        TablePosition pos = files.getSelectionModel().getSelectedCells().get(0);
        int row = pos.getRow();
        FileController item = files.getItems().get(row);
        TableColumn col = pos.getTableColumn();
        selectedFile = (String) col.getCellObservableValue(item).getValue();
    }

    /**
     * Добавление информации о каталоге файлов в строки таблицы, которая отображает
     * каталог файлов
     */
    private void initData() {
        for (File file : Objects.requireNonNull(new File(selectedDir).listFiles())) {
            userData.add((new FileController(file.getAbsolutePath())));
        }
    }

    /**
     * Метод для получения введённых данных из текстового поля
     */
    @FXML
    private void handleButton1Action() { //Переименовать метод
        selectedDir = pathArea.getText();
        refreshDirectory();
    }

    /**
     * @param currentPath текущий путь
     *                    Метод для отображения выбранного пользователем файла для
     *                    произведения с ним манипуляций
     */
    @FXML
    private void selectedFileLabelInit(File currentPath) {
        selectedFileLabel.setText("SELECTED: " + currentPath.getName());
    }

    /**
     * Переход вверх на один каталог
     */
    @FXML
    private void handleBackButton() {
        Path pathToDir = Paths.get(selectedDir);
        nullCheck(pathToDir);
        refreshDirectory();
    }

    /**
     * @param path путь к текущему каталогу
     *             Метод для проверки, является ли текущий путь null'ом
     */
    private void nullCheck(Path path) {
        if (path.getParent() != null) {
            selectedDir = String.valueOf(path.getParent());
        }
    }

    /**
     * Метод для кнопки удаления. При нажатии на кнопку вызывается метод для удаления выбранного файла и
     * методы для диалога с пользователем
     */
    @FXML
    private void handleDeleteButton() {
        int selectedIndex = files.getSelectionModel().getSelectedIndex();
        if (selectedIndex != -1) {
            FileController fileControl = new FileController(selectedFile);

            Optional<ButtonType> result = DeleteButtonAlerts.deleteConfirm();
            if (result.get() == ButtonType.OK) {
                if (Files.isDirectory(Paths.get(selectedFile))) {
                    Delete.dirDelete(fileControl);
                    DeleteButtonAlerts.deleteDirInformation();
                } else {
                    Delete.oneFileDelete(fileControl);
                    DeleteButtonAlerts.deleteFileInformation();
                }
            }
            refreshDirectory();
        } else   DeleteButtonAlerts.deleteWarning();
    }

    /**
     * Метод для кнопки шифрования. При нажатии на кнопку вызывается метод для шифрования выбранного файла и
     * методы для диалога с пользователем
     */
    @FXML
    private void handleEncryptButton() {
        int selectedIndex = files.getSelectionModel().getSelectedIndex();
        if (selectedIndex != -1) {
            FileController fileControl = new FileController(selectedFile);

            Optional<ButtonType> result = EncryptButtonAlerts.encryptConfirm();
            if (result.get() == ButtonType.OK) {
                if (Files.isRegularFile(Paths.get(selectedFile))) {
                    FileCrypting.fileEncrypt(fileControl);
                    EncryptButtonAlerts.encryptInformation();
                }
            }
            refreshDirectory();
        } else   EncryptButtonAlerts.encryptWarning();
    }

    /**
     * Метод для кнопки расшифрования. При нажатии на кнопку вызывается метод для расшифровки выбранного файла и
     * методы для диалога с пользователем
     */
    @FXML
    private void handleDecryptButton() {
        int selectedIndex = files.getSelectionModel().getSelectedIndex();
        if (selectedIndex != -1) {
            FileController fileControl = new FileController(selectedFile);

            Optional<ButtonType> result = DecryptButtonAlerts.decryptConfirm();
            if (result.get() == ButtonType.OK) {
                if (Files.isRegularFile(Paths.get(selectedFile))) {
                    FileCrypting.fileDecrypt(fileControl);
                    DecryptButtonAlerts.decryptInformation();
                }
            }
            refreshDirectory();
        } else DecryptButtonAlerts.decryptWarning();
    }

    /**
     * Метод для кнопки архивации. При нажатии на кнопкку вызываетсяя метод для архивации выбранного файла и
     * методы для диалога с пользователем
     */
    @FXML
    private void handleArchiveButton() {
        int selectedIndex = files.getSelectionModel().getSelectedIndex();
        if (selectedIndex != -1) {
            FileController fileControl = new FileController(selectedFile);

            Optional<ButtonType> result = ArchiveButtonAlerts.archiveConfirm();
            if (result.get() == ButtonType.OK) {
                try {
                    Archive.zipping(fileControl);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (Files.isDirectory(Paths.get(selectedFile))) {
                    ArchiveButtonAlerts.archiveDirInformation();
                } else {
                    ArchiveButtonAlerts.archiveFileInformation();
                }
            }
            refreshDirectory();
        } else  ArchiveButtonAlerts.archiveWarning();
    }

    /**
     * Метод для кнопки разархивации. При нажатии на кнопкку вызываетсяя метод для разархивации выбранного Zip файла и
     * методы для диалога с пользователем
     */
    @FXML
    private void handleUnarchiveButton() {
        int selectedIndex = files.getSelectionModel().getSelectedIndex();
        if (selectedIndex != -1) {
            FileController fileController = new FileController(selectedFile);

            Optional<ButtonType> result = UnarchiveButtonAlerts.unarchiveConfirm();
            if (result.get() == ButtonType.OK) {
                Unarchive.unZip(fileController);
                UnarchiveButtonAlerts.unarchiveInformation();
            }
            refreshDirectory();
        } else UnarchiveButtonAlerts.unarchiveWarning();
    }

    /**
     * Метод для обновления текущего отображения каталога файлов
     */
    @FXML
    private void refreshDirectory() {
        userData.clear();
        initialize();
    }

    /**
     * Метод для создания нового окна интерфейса, в котором отображается новый каталог файлов
     */
    @FXML
    private void handleNewWindowButtonAction() {
        int selectedIndex = files.getSelectionModel().getSelectedIndex();
        if (selectedIndex != -1) {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/NewFileCatalog.fxml"));
            try {
                Parent newWindow = fxmlLoader.load();
                Stage stage = new Stage();
                stage.setScene(new Scene(newWindow));
                stage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else NewWindowButtonAlerts.newFileWarning();
    }

    /**
     * Метод для создания нового окна интерфейса, в котором нужно ввести имя файла
     */
    @FXML
    private void handleRenameWindowButtonAction() {
        int selectedIndex = files.getSelectionModel().getSelectedIndex();
        if (selectedIndex != -1) {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/RenameFile.fxml"));
            try {
                Parent newWindow = fxmlLoader.load();
                Stage stage = new Stage();
                stage.setScene(new Scene(newWindow));
                stage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else RenameButtonAlerts.renameWarning();
    }

    /**
     * Метод для создания нового окна интерфейса, в котором нужно ввести имя файла для создания файла/директории
     */
    @FXML
    private void handleCreateWindowButtonAction() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/CreateFileOrDir.fxml"));
        try {
            Parent newWindow = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(newWindow));
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}