package controller.utility;

import controller.utility.alerts.fm.CopyButtonAlerts;
import controller.utility.alerts.fm.MoveButtonAlerts;
import controller.utility.tocopy.fm.Copy;
import controller.utility.tomove.fm.Move;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;


import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.Optional;

/**
 * Класс, предназначен для работы с интерфейсом и локальной базой данных
 *
 * Функционал методов для работы с исходным кодом:
 * <ul>
 *     <li>Инициализация каталога файлов, поддержание его актуальности</li>
 *     <li>Методы, производящие некие действия при нажатии на определенные части интерфейса</li>
 * </ul>
 *
 * @author Проничкин Вячеслав, 16IT18k
 */

public class NewFileCatalogController {

    //Создание локальной базы данных из каталога файлов по текущему пути
    private ObservableList<FileController> usersData = FXCollections.observableArrayList();

    //Выбранный путь для просмотра каталога файлов
    private String selectedDir = "E:\\";

    //Выбранный файл для произведения с ним различных манипуляций
    public static String selectedFile;

    //Переменная для связи с Label, чтобы отображать выбранный файл
    @FXML
    private Label selectedFileLabel;

    //Переменная для связи с текстовым полем
    @FXML
    TextField pathArea;

    //Переменная для связи с таблицей
    @FXML
    private TableView<FileController> files;

    //Переменная для связи со столбцами в таблице
    @FXML
    private TableColumn<FileController, String> nameFile;

    /**
     *  Метод для инициализации каталога файлов, а так же поддержания его актуальности
     *  Под актуальностью имеется ввиду отображение нового каталога файлов при изменении
     *  текущего абсолютного пути
     */
    @FXML
    private void initialize() {
        initData();
        nameFile.setCellValueFactory(new PropertyValueFactory<>("file"));

        files.setItems(usersData);
        clickEvent();
    }

    /**
     * Метод для перехода во вложенную папку по двойному щелчку или выбор
     * файла для манипуляций с ним по одиночному щелчку
     */
    private void clickEvent() {
        files.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
                    if (mouseEvent.getClickCount() == 2) {
                        if (files.getSelectionModel().getSelectedItem() != null) {
                            getSelectedRow();
                            File currentPath = new File(selectedFile);
                            if (Files.isDirectory(Paths.get(selectedDir + "\\" + currentPath.getName()))) {
                                selectedDir = selectedDir + "\\" + currentPath.getName();
                                refreshDirectory();
                            }
                        }
                    } else {
                        if (files.getSelectionModel().getSelectedItem() != null) {
                            getSelectedRow();
                            File currentPath = new File(selectedFile);
                            selectedFileLabelInit(currentPath);
                        }
                    }
                }
            }
        });
    }


    /**
     * Нахождение и получение той информации, которая находится в ячейке таблицы,
     * на которую кликнули мышкой.
     */
    private void getSelectedRow() {
        TablePosition pos = files.getSelectionModel().getSelectedCells().get(0);
        int row = pos.getRow();
        FileController item = files.getItems().get(row);
        TableColumn col = pos.getTableColumn();
        selectedFile = (String) col.getCellObservableValue(item).getValue();
    }

    /**
     * Добавление информации о каталоге файлов в строки таблицы, которая отображает
     * каталог файлов
     */
    private void initData() {
        for (File file : Objects.requireNonNull(new File(selectedDir).listFiles())) {
            if (file.isDirectory()) {
                usersData.add((new FileController(file.getAbsolutePath())));
            } else {

            }
        }
    }

    /**
     * @param currentPath текущий путь до текущего каталога
     *                    Метод для вывода данныз в Label, отображение выбранного каталога.
     */
    @FXML
    private void selectedFileLabelInit(File currentPath) {
        selectedFileLabel.setText("Selected Directory : " + currentPath.getName());
    }

    /**
     * Переход вверх на один каталог
     */
    @FXML
    private void handleBackButton() {
        Path pathToDir = Paths.get(selectedDir);
        nullCheck(pathToDir);
        refreshDirectory();
    }

    /**
     * @param path путь к текущему каталогу
     *             Метод для проверки, является ли текущий путь null'ом
     */
    private void nullCheck(Path path) {
        if (path.getParent() != null) {
            selectedDir = String.valueOf(path.getParent());
        }
    }

    /**
     * Метод для обновления текущего отображения каталога файлов
     */
    private void refreshDirectory() {
        usersData.clear();
        initialize();
    }

    @FXML
    private javafx.scene.control.Button exitButton, moveButton, copyButton;

    /**
     * Перемещение файлов
     */
    @FXML
    private void handleMoveButton() {
        int selectedIndex = files.getSelectionModel().getSelectedIndex();
        if (selectedIndex != -1) {
            Controller.selectedDestinationPath = selectedFile;
            FileController fileControl = new FileController(Controller.selectedFile);
            fileControl.setDestination(Controller.selectedDestinationPath);

            Optional<ButtonType> result = MoveButtonAlerts.moveConfirm();
            if (result.get() == ButtonType.OK) {
                if (Files.isDirectory(Paths.get(Controller.selectedFile))) {
                    Move.dirMoving(fileControl);
                    MoveButtonAlerts.moveDirInformation();
                } else {
                    Move.oneFileMoving(fileControl);
                    MoveButtonAlerts.moveFileInformation();
                }
            }
            refreshDirectory();
            Controller.selectedDestinationPath = null;
            windowClose(copyButton);
        } else MoveButtonAlerts.moveWarning();
    }

    /**
     * Копирование файла
     */
    @FXML
    private void handleCopyButton() {
        int selectedIndex = files.getSelectionModel().getSelectedIndex();
        if (selectedIndex != -1) {
            Controller.selectedDestinationPath = selectedFile;
            FileController fileControl = new FileController(Controller.selectedFile);
            fileControl.setDestination(Controller.selectedDestinationPath);

            Optional<ButtonType> result = CopyButtonAlerts.copyConfirm();
            if (result.get() == ButtonType.OK) {
                Copy.copyFile(fileControl);
                CopyButtonAlerts.copyInformation();
            }
            refreshDirectory();
            Controller.selectedDestinationPath = null;
            windowClose(copyButton);
        } else CopyButtonAlerts.copyWarning();
    }

    /**
     * Закрытие окна интерфейса
     */
    @FXML
    private void handleExitButton() {
        windowClose(exitButton);
    }

    /**
     * @param button fx:id кнопки
     *
     */
    private void windowClose(javafx.scene.control.Button button) {
        Stage stage = (Stage) button.getScene().getWindow();
        stage.close();
    }
}