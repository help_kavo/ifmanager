package controller.utility.torename.fm;

import controller.utility.FileController;

import java.io.IOException;
import java.nio.file.*;
/**
 * Класс для переименования файла или папки
 *
 * Функционал методов для работы с исходным кодом:
 * <ul>
 * <li> перезаписывает файл с новым именем </li>
 * </ul>
 *
 * @author Ворожейкин Александр, 16IT18k
 */
public class Rename {

    /**
     * @param paths Объект класса FileController, который содержит в себе путь таргета и путь назначения.
     * @param newFileName Новое имя файла
     */
    public static void rename(FileController paths, String newFileName){
        try {
            Files.move(paths.getPath(), Paths.get(paths.getPath().getParent() + "\\" + newFileName), StandardCopyOption.REPLACE_EXISTING);
        }catch (IOException e){
            e.printStackTrace();
            System.exit(1);
        }
    }
}
