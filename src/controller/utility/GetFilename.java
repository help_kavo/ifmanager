package controller.utility;

public class GetFilename {

    /**
     * Класс предназначен для разделения имени файла на куски,
     * а точнее - на базовое имя файла и его расширение.
     */
    public GetFilename() {
    }

    public static String filename(FileController paths) { // gets filename without extension
        int dot = paths.getFile().lastIndexOf('.');
        int sep = paths.getFile().lastIndexOf('/');
        return paths.getFile().substring(sep + 1, dot);
    }
}
