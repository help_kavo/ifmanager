import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class Demo extends Application {

    @Override
    public void start(Stage primaryStage) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/MainWindow.fxml"));
        primaryStage.setTitle("IFManager");
        primaryStage.setScene(new Scene(root, 735, 550));
        primaryStage.show();
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}
