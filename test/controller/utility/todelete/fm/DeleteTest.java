package controller.utility.todelete.fm;

import controller.utility.FileController;
import controller.utility.toarchive.fm.Archive;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DeleteTest {

    /**
     * Тестирование класса Delete.oneFileDelete(), проверка допустимых названий файлов и путей.
     */
    @Test
    void oneFileDelete() {
        String[] namesToCheck = {"testDirectory",
                "newName",
                "#...251",
                "//new//aa",
                "/new/new",
                "",
                null,
                "file.randomExtension",
                "file...........",
                "sfdksdsijfioksdfdsnmjfsdifmjsdiofmokidsfmokisdfksmdjkfmsdkfmskdlfmklsdfmnklsdkflsdklfskldfklsdnfklnsdklfnsdklfnklsdnfklsdnfklsnklfsnkldfnlksfnlksnfslflsfnsldfn",
                "{}{}`1`",
                "<?>",
                "\\F",
                "みんなの日本語",
                "~ \"# % & *: < >? / \\ { | }",
                "\"*: < >? / \\ |",
                ".lock",
                "desktop.ini",
                "~$name"

        };

        for (int i = 0; i < 19; i++) {

            String path = "C:\\filesTesting\\FilesCreateTestDir\\" + namesToCheck[i];
            FileController pathsToCheck = new FileController(path);

            try {
                Delete.oneFileDelete(pathsToCheck);
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    /**
     * Тестирование класса Delete.dirDelete(), проверка допустимых названий файлов и путей.
     */
    @Test
    void dirDelete() {
        String[] namesToCheck = {"testDirectory",
                "newName",
                "#...251",
                "//new//aa",
                "/new/new",
                "",
                null,
                "file.randomExtension",
                "file...........",
                "sfdksdsijfioksdfdsnmjfsdifmjsdiofmokidsfmokisdfksmdjkfmsdkfmskdlfmklsdfmnklsdkflsdklfskldfklsdnfklnsdklfnsdklfnklsdnfklsdnfklsnklfsnkldfnlksfnlksnfslflsfnsldfn",
                "{}{}`1`",
                "<?>",
                "\\F",
                "みんなの日本語",
                "~ \"# % & *: < >? / \\ { | }",
                "\"*: < >? / \\ |",
                ".lock",
                "desktop.ini",
                "~$name"

        };

        for (int i = 0; i < 19; i++) {

            String path = "C:\\filesTesting\\ArchiveTestingDir\\" + namesToCheck[i];
            FileController pathsToCheck = new FileController(path);

            try {
                Delete.dirDelete(pathsToCheck);
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}