package controller.utility.toarchive.fm;

import controller.utility.FileController;
import org.junit.jupiter.api.Test;

import java.io.File;

class UnarchiveTest {

    /**
     * <ul>
     * <li>Тестирование метода unZip класса Unarchive</li>
     * </ul>
     *
     * Проверка допустимых названий файлов и различные тестовые комбинации для
     * разархивирования Zip-файла
     * Тестовые комбинации берутся из директории по пути "E:\\filesTesting\\UnarchiveTestingDir\\",
     * путь хранится в константе PATH_TO_TEST
     */
    @Test
    void unZip() {
        final String PATH_TO_TEST = "E:\\filesTesting\\UnarchiveTestingDir\\";

        File dir = new File(PATH_TO_TEST);
        File[] arrayOfFiles = dir.listFiles();

        assert arrayOfFiles != null;
        for (File files : arrayOfFiles) {

            FileController pathsToCheck = new FileController(files.getPath());

            try {
                Unarchive.unZip(pathsToCheck);
                System.out.println("Zip-файл " + pathsToCheck.getFile() + " успешно разархивирован.");
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}