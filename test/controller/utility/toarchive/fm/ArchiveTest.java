package controller.utility.toarchive.fm;

import controller.utility.FileController;
import org.junit.jupiter.api.Test;

import java.io.File;

class ArchiveTest {

    /**
     * <ul>
     * <li>Тестирование метода zipping класса Archive</li>
     * </ul>
     *
     * Проверка допустимых названий файлов и различные тестовые комбинации для
     * архивирования как файла, так и директории
     * Тестовые комбинации берутся из директории по пути "E:\\filesTesting\\UnarchiveTestingDir\\",
     * путь хранится в константе PATH_TO_TEST
     */
    @Test
    void zipping() {
        final String PATH_TO_TEST = "E:\\filesTesting\\ArchiveTestingDir\\";

        File dir = new File(PATH_TO_TEST);
        File[] arrayOfFiles = dir.listFiles();

        assert arrayOfFiles != null;
        for (File files : arrayOfFiles) {

            FileController pathsToCheck = new FileController(files.getPath());

            try {
                Archive.zipping(pathsToCheck);
                System.out.println("Файл " + pathsToCheck.getFile() + " успешно архивирован.");
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}