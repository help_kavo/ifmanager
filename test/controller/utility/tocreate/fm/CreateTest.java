package controller.utility.tocreate.fm;

import controller.utility.FileController;
import controller.utility.torename.fm.Rename;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class CreateTest {

    /**
     * Тестирование класса Create.oneFileCreation(), проверка допустимых названий файлов и путей.
     */
    @Test
    void oneFileCreation() {

        String path = "C:\\filesTesting\\FilesCreateTestDir";
        FileController pathsToCheck = new FileController(path);

        String[] namesToCreate = {"testDirectory",
                "newName",
                "#...251",
                "//new//aa",
                "/new/new",
                "",
                null,
                "file.randomExtension",
                "file...........",
                "sfdksdsijfioksdfdsnmjfsdifmjsdiofmokidsfmokisdfksmdjkfmsdkfmskdlfmklsdfmnklsdkflsdklfskldfklsdnfklnsdklfnsdklfnklsdnfklsdnfklsnklfsnkldfnlksfnlksnfslflsfnsldfn",
                "{}{}`1`",
                "<?>",
                "\\F",
                "みんなの日本語",
                "~ \"# % & *: < >? / \\ { | }",
                "\"*: < >? / \\ |",
                ".lock",
                "desktop.ini",
                "~$name"

        };

        for (int i = 0; i < 19; i++) {
            try {
                Create.oneFileCreation(pathsToCheck, namesToCreate[i]);
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    /**
     * Тестирование класса Create.onePackageCreate(), проверка допустимых названий файлов и путей.
     */
    @Test
    void onePackageCreate() {

        String path = "C:\\filesTesting\\DirectoryCreateTestDir";
        FileController pathsToCheck = new FileController(path);

        String[] namesToCreate = {"testDirectory",
                "newName",
                "#...251",
                "//new//aa",
                "/new/new",
                "",
                null,
                "file.randomExtension",
                "file...........",
                "sfdksdsijfioksdfdsnmjfsdifmjsdiofmokidsfmokisdfksmdjkfmsdkfmskdlfmklsdfmnklsdkflsdklfskldfklsdnfklnsdklfnsdklfnklsdnfklsdnfklsnklfsnkldfnlksfnlksnfslflsfnsldfn",
                "{}{}`1`",
                "<?>",
                "\\F",
                "みんなの日本語",
                "~ \"# % & *: < >? / \\ { | }",
                "\"*: < >? / \\ |",
                ".lock",
                "desktop.ini",
                "~$name"

        };

        for (int i = 0; i < 19; i++) {
            try {
                Create.onePackageCreate(pathsToCheck, namesToCreate[i]);
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}