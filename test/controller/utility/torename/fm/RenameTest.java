package controller.utility.torename.fm;

import controller.utility.FileController;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

class RenameTest {

    /**
     * Тестирование класса Rename.rename(), проверка допустимых названий файлов и путей.
     */
    @Test
    void rename() {

        String path = "C:\\filesTesting\\TestDirectory";
        FileController pathsToCheck = new FileController(path);

        String[] namesToRename = {"testDirectory",
                "newName",
                "#...251",
                "//new//aa",
                "/new/new",
                "",
                null,
                "file.randomExtension",
                "file...........",
                "sfdksdsijfioksdfdsnmjfsdifmjsdiofmokidsfmokisdfksmdjkfmsdkfmskdlfmklsdfmnklsdkflsdklfskldfklsdnfklnsdklfnsdklfnklsdnfklsdnfklsnklfsnkldfnlksfnlksnfslflsfnsldfn",
                "{}{}`1`",
                "<?>",
                "\\F",
                "みんなの日本語",
                "~ \"# % & *: < >? / \\ { | }",
                "\"*: < >? / \\ |",
                ".lock",
                "desktop.ini",
                "~$name"

        };

        for(int i = 0; i < 19; i++){
            Rename.rename(pathsToCheck, namesToRename[i]);

        }
    }
}