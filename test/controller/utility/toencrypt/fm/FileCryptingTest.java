package controller.utility.toencrypt.fm;

import controller.utility.FileController;
import controller.utility.todelete.fm.Delete;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FileCryptingTest {

    /**
     * Тестирование класса FileCrypting.fileDecrypt(), проверка допустимых названий файлов и путей.
     */
    @Test
    void fileEncrypt() {
        String[] namesToCheck = {"testDirectory",
                "newName",
                "#...251",
                "//new//aa",
                "/new/new",
                "",
                null,
                "file.randomExtension",
                "file...........",
                "sfdksdsijfioksdfdsnmjfsdifmjsdiofmokidsfmokisdfksmdjkfmsdkfmskdlfmklsdfmnklsdkflsdklfskldfklsdnfklnsdklfnsdklfnklsdnfklsdnfklsnklfsnkldfnlksfnlksnfslflsfnsldfn",
                "{}{}`1`",
                "<?>",
                "\\F",
                "みんなの日本語",
                "~ \"# % & *: < >? / \\ { | }",
                "\"*: < >? / \\ |",
                ".lock",
                "desktop.ini",
                "~$name"

        };

        for (int i = 0; i < 19; i++) {

            String path = "C:\\filesTesting\\EncryptingTestDir\\" + namesToCheck[i];
            FileController pathsToCheck = new FileController(path);

            try {
                FileCrypting.fileEncrypt(pathsToCheck);
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    /**
     * Тестирование класса FileCrypting.fileEncrypt(), проверка допустимых названий файлов и путей.
     */
    @Test
    void fileDecrypt() {
        String[] namesToCheck = {"testDirectory",
                "newName",
                "#...251",
                "//new//aa",
                "/new/new",
                "",
                null,
                "file.randomExtension",
                "file...........",
                "sfdksdsijfioksdfdsnmjfsdifmjsdiofmokidsfmokisdfksmdjkfmsdkfmskdlfmklsdfmnklsdkflsdklfskldfklsdnfklnsdklfnsdklfnklsdnfklsdnfklsnklfsnkldfnlksfnlksnfslflsfnsldfn",
                "{}{}`1`",
                "<?>",
                "\\F",
                "みんなの日本語",
                "~ \"# % & *: < >? / \\ { | }",
                "\"*: < >? / \\ |",
                ".lock",
                "desktop.ini",
                "~$name"

        };

        for (int i = 0; i < 19; i++) {

            String path = "C:\\filesTesting\\EncryptingTestDir\\" + namesToCheck[i] + ".encrypted";
            FileController pathsToCheck = new FileController(path);

            try {
                FileCrypting.fileEncrypt(pathsToCheck);
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}